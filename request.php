<?php
session_start();
header('Access-Control-Allow-Origin: *');
header("Content-Type: application/json");
header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, HEAD, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
include_once 'conexion.php';

$request_method = $_SERVER['REQUEST_METHOD'];
switch ($request_method) {
    case 'GET':
        $sql = $mysqli -> query("SELECT user_id, id_file, name, extension as type, size, username FROM users AS u INNER JOIN files AS f ON u.user_id = f.id_user;");
        if (!$sql) {
            die();
        } else {
            foreach ($sql as $row) {
                $data[] = $row;
            }
            $response = [
                'data' => $data,
                'status' => 200
            ];
            echo json_encode($response);
        }
        break;
    case 'POST':
        $json = file_get_contents('php://input');
        $obj = json_decode($json, true);
        
        $operation = $obj['operation'];
        switch ($operation) {
            case 'logout':
                session_destroy();
                $response = [
                    'logged' => false
                ];
                echo json_encode($response);
                break;
            case 'check':
                if (isset($_SESSION['paterno'])) {
                    $response = [
                        'logged' => true,
                        'name' => $_SESSION['name'],
                        'paterno' => $_SESSION['paterno'],
                        'materno' => $_SESSION['materno'],
                        'user' => $_SESSION['user'],
                        'user_id' => $_SESSION['user_id']
                    ];
                } else {
                    $response = [
                        'logged' => false
                    ];
                }
                echo json_encode($response);
                break;
            case 'login':
                $email = $obj['email'];
                $password = $obj['password'];
                $sql = $mysqli -> query("SELECT * FROM users WHERE email='$email' AND password='$password';");
                if (!$sql) {
                    die();
                } else {
                    foreach ($sql as $row) {
                        $data[] = $row;
                        $name = $row['name'];
                        $paterno = $row['paterno'];
                        $materno = $row['materno'];
                        $user = $row['user'];
                        $user_id = $row['user_id'];
                    }
                    if($data) {
                        $_SESSION['name'] = $name;
                        $_SESSION['paterno'] = $paterno;                                                             
                        $_SESSION['materno'] = $materno; 
                        $_SESSION['user'] = $user; 
                        $_SESSION['user_id'] = $user_id; 
                    }
                    $response = [
                        'data' => $data,
                        'logged' => true
                    ];
                    echo json_encode($response);
                }
                break;
            case 'save':
                $name = $obj['name'];
                $paterno = $obj['paterno'];
                $materno = $obj['materno'];
                $user = $obj['user'];
                $email = $obj['email'];
                $password = $obj['password'];
                $sql = $mysqli -> query("INSERT INTO users values(
                    0,
                    '$name',
                    '$paterno',
                    '$materno',
                    '$user',
                    '$email',
                    '$password',
                    now(),
                    now()
                    );");
                if (!$sql) {
                    $response = ['insert_status' => $sql];
                    echo json_encode($response);
                } else {
                    $user_id = mysqli_insert_id($mysqli);
                    $_SESSION['name'] = $name;
                    $_SESSION['paterno'] = $paterno;                                                             
                    $_SESSION['materno'] = $materno; 
                    $_SESSION['user'] = $user; 
                    $_SESSION['user_id'] = $user_id; 
                    $response = [
                        'insert_status' => $sql,
                        'user_id' => $user_id
                    ];
                    echo json_encode($response);
                }
                break;
            case 'save-result':
                $user_id = $obj['user_id'];
                $expression = $obj['expression'];
                $type = $obj['type'];
                $result = $obj['result'];
                $num_variables = $obj['num_variables'];
                $hit = $obj['hit'];
                $wrong = $obj['wrong'];
                $score = $obj['score'];
                $points = $obj['points'];
                $user_response = $obj['user_response'];
                $compiler_response = $obj['compiler_response'];
                $sql = $mysqli -> query("INSERT INTO results values(
                    0,
                    $user_id,
                    '$expression',
                    '$type',
                    '$result',
                    '$num_variables',
                    '$hit',
                    '$wrong',
                    $score,
                    $points,
                    '$user_response',
                    '$compiler_response',
                    now(),
                    now());");
                if (!$sql) {
                    $response = ['insert_status' => $sql];
                    echo json_encode($response);
                } else {
                    $response = [
                        'insert_status' => $sql,
                        'result_id' => mysqli_insert_id($mysqli)
                    ];
                    echo json_encode($response);
                }
            break;
            case 'get-user-results':
                $sql = $mysqli -> query("SELECT name, paterno, materno, user, r.points, r.num_expressions, r.total_score
                FROM users as u inner join 
                (SELECT user_id, sum(points) as points, count(expression) as num_expressions, sum(score) as total_score
                from results 
                group by user_id) as r on u.user_id=r.user_id order by r.points desc;");
                if (!$sql) {
                    die();
                } else {
                    foreach ($sql as $row) {
                        $data[] = $row;
                    }
                    $response = [
                        'data' => $data,
                        'status' => 200
                    ];
                    echo json_encode($response);
                }
            break;
            case 'get-user-result':
                $user = $obj['user'];
                $sql = $mysqli -> query("SELECT * FROM results as r 
                inner join (SELECT user_id from users where user='$user') as u on r.user_id=u.user_id;");
                if (!$sql) {
                    die();
                } else {
                    foreach ($sql as $row) {
                        $data[] = $row;
                    }
                    $response = [
                        'data' => $data,
                        'status' => 200
                    ];
                    echo json_encode($response);
                }
            break;
            case 'select_scores':
                $score_id = $obj['score_id'];
                $sql = $mysqli -> query("SELECT * FROM scores WHERE score_id='$score_id';");
                if (!$sql) {
                    $response = ['select_status' => $sql];
                    echo json_encode($response);
                    die();
                } else {
                    foreach ($sql as $row) {
                        $data[] = $row;
                    }
                    $response = [
                        'data' => $data,
                        'select_status' => $sql
                    ];
                    echo json_encode($response);
                }
            break;
            case 'update_score':
                $score_id = $obj['score_id'];
                $var1 = $obj['var1'];
                $var2 = $obj['var2'];
                $var3 = $obj['var3'];
                $var4 = $obj['var4'];
                $var5 = $obj['var5'];
                $var6 = $obj['var6'];
                $var7 = $obj['var7'];
                $sql = $mysqli -> query("UPDATE scores SET 
                one_var='$var1',
                two_var='$var2',
                three_var='$var3',
                four_var='$var4',
                five_var='$var5',
                six_var='$var6',
                seven_var='$var7' WHERE score_id='$score_id';");
                if (!$sql) {
                    $response = ['update_status' => $sql];
                    echo json_encode($response);
                } else {
                    $response = [
                        'update_status' => $sql
                    ];
                    echo json_encode($response);
                }
            break;
            case 'delete-files':
                $user_id=$obj['user_id'];
                $sql = $mysqli -> query("DELETE FROM files WHERE id_user=$user_id");
                if (!$sql) {
                    $response = ['insert_status' => $sql];
                    echo json_encode($response);
                } else {
                    $response = [
                        'insert_status' => $sql
                    ];
                    echo json_encode($response);
                }
                break;   
            default:
                # code...
                break;
        }
        break;
    default:
        echo "hola mundo";
        break;
}
?>
